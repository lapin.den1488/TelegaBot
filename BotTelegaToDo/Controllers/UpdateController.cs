﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BotTelegaToDo.Models;
using BotTelegaToDo.Services;
using BotTelegaToDo.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Telegram.Bot.Types;

namespace BotTelegaToDo.Controllers
{
    [Route(@"api/update")]
    public class UpdateController : Controller
    {
        private readonly IUsageService service;
        public UpdateController(IUsageService service){
            this.service = service;
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody]Update update)
        {
            var result = await CommandExecutor.Execute(update,service);  
            if(result)
                return Ok();
            
            return BadRequest(); 
        }
    }
}
