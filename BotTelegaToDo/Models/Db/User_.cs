using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BotTelegaToDo.Models.Db{
    public class User_{
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.None)]
        public int Id { get; set; }
        public string Username { get; set; }
        public List<Task_> Tasks{get;set;}

        public User_(){
            Tasks = new List<Task_>();
        }
        
        public User_(int Id, string Username){
            this.Id = Id;
            this.Username = Username;
            this.Tasks = new List<Task_>();
        }
    }
}