using Microsoft.EntityFrameworkCore;

namespace BotTelegaToDo.Models.Db
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options) { }
        public DbSet<User_> Users { get; set; }
        public DbSet<Task_> Tasks { get; set; }
    }
}