using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BotTelegaToDo.Models.Db{
    public class Task_{
        
        [Key]
        public int Id { get; set; }                
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime FinishedDate { get; set; }
        public int User_Id{get;set;}
        public User_ User{get;set;}

        public Task_(){}
        
        public Task_(string Name, DateTime FinishedDate){
            this.Name = Name;
            this.FinishedDate = FinishedDate;
            this.Description = Description;
            this.CreatedDate = DateTime.Now;
        }
    }
}