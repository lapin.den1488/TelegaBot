using System.Collections.Generic;
using System.Threading.Tasks;
using BotTelegaToDo.Services.Interfaces;
using Telegram.Bot;
using Telegram.Bot.Types;

namespace BotTelegaToDo.Models.Commnands{
    public abstract class Command{
        public abstract string Name { get; }

        public abstract Task ExecuteAsync(Message message, TelegramBotClient client,IUsageService service);

        public abstract bool Contains(Message message);        
    }
}