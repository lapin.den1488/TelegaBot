using System.Collections.Generic;
using System.Threading.Tasks;
using BotTelegaToDo.Services.Interfaces;
using Telegram.Bot;
using Telegram.Bot.Types;

namespace BotTelegaToDo.Models.Commnands{
    public class DeleteCommand : Command
    {
        public override string Name => @"/delete";

        public override bool Contains(Message message)
        {
            if (message.Type != Telegram.Bot.Types.Enums.MessageType.Text)
                return false;

            return message.Text.Contains(this.Name);
        }

        public override async Task ExecuteAsync(Message message, TelegramBotClient client,IUsageService service)
        {
            await service.DeleteTask(message,client);
        }
    }
}