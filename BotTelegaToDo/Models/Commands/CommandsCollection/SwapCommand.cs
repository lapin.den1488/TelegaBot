using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types;
using BotTelegaToDo.Models;
using BotTelegaToDo.Services.Interfaces;
using System.Collections.Generic;

namespace BotTelegaToDo.Models.Commnands.CommandsCollection{
    public class SwapCommand : Command
    {        
        public override string Name => @"/swap";

        public override bool Contains(Message message)
        {
           if (message.Type != Telegram.Bot.Types.Enums.MessageType.Text)
                return false;

            return message.Text.Contains(this.Name);
        }

        public override async Task ExecuteAsync(Message message, TelegramBotClient client,IUsageService service)
        {
            await service.SwapTasks(message,client);
        }
    }
}