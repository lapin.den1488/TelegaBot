using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types;
using BotTelegaToDo.Models;
using BotTelegaToDo.Services.Interfaces;
using System.Collections.Generic;

namespace BotTelegaToDo.Models.Commnands.CommandsCollection{
    public class EditTaskCommand : Command
    {        
        public override string Name => @"/edit";

        public override bool Contains(Message message)
        {
           if (message.Type != Telegram.Bot.Types.Enums.MessageType.Text)
                return false;

            return message.Text.Contains(this.Name);
        }

        public override async Task ExecuteAsync(Message message, TelegramBotClient client, IUsageService service)
        {
            await service.EditTask(message,client);
        }
    }
}