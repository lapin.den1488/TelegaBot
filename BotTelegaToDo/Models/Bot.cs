using System.Collections.Generic;
using System.Threading.Tasks;
using BotTelegaToDo.Models.Commnands;
using BotTelegaToDo.Models.Commnands.CommandsCollection;
using BotTelegaToDo.Services.Interfaces;
using Microsoft.ApplicationInsights.Extensibility;
using Microsoft.Extensions.Configuration;
using Telegram.Bot;

namespace BotTelegaToDo.Models{
   public class Bot
    {
        private static Bot bot;
        public TelegramBotClient botClient;
        public  List<Command> commandsList;

        private Bot(){}
        
        public static async Task<Bot> GetInstance(){
            if(bot != null)
                return bot;
            
            bot = new Bot();
            bot.botClient = await GetBotClientAsync();
            bot.commandsList = GetCommands();

            return bot;            
        }

        private static List<Command> GetCommands()
        {
            List<Command>Commands = new List<Command>();            
            Commands.Add(new StartCommand());
            Commands.Add(new AddTaskCommand());
            Commands.Add(new ShowTasksCommand());            
            Commands.Add(new SwapCommand()); 
            Commands.Add(new DeleteCommand());
            Commands.Add(new EditTaskCommand());
            Commands.Add(new HelpCommand());  

            return Commands;
        } 

        private static async Task<TelegramBotClient> GetBotClientAsync()
        {            
            var client = new TelegramBotClient(BotConfiguration.Key);

            string hook = string.Format(BotConfiguration.Url, "api/update");            
            await client.SetWebhookAsync(hook);
            
            return client;
        }
    }
}