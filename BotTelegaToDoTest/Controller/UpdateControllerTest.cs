using Xunit;
using BotTelegaToDo.Controllers;
using Moq;
using BotTelegaToDo.Services;
using BotTelegaToDo.Services.Interfaces;
using Telegram.Bot.Types;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace BotTelegaToDoTest
{    
    public class UpdateControllerTest
    {        
        [Fact]
        public async Task PostOkTesting()
        {
            var moq = new Mock<IUsageService>();
            UpdateController controller = new UpdateController(moq.Object);

            Update update = new Update();
            Message message = new Message();
            message.Text = @"/help";
            update.Message = message;

            var result = await controller.Post(update);

            Assert.IsType<OkResult>(result);
        }
        [Fact]
        public async Task PostBadRequestTesting()
        {
            var moq = new Mock<IUsageService>();
            UpdateController controller = new UpdateController(moq.Object);

            Update update = new Update();
            Message message = new Message();
            message.Text = It.IsAny<string>();
            update.Message = message;

            var result = await controller.Post(update);

            Assert.IsType<BadRequestResult>(result);
        }
    }
}